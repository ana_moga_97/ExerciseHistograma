
import java.util.Comparator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ana
 */
public class OccurenceComparator implements Comparator<CharOccurence> { 
     public int compare(CharOccurence e1, CharOccurence e2) {
        return e1.getOcc() - e2.getOcc();
    }
}
