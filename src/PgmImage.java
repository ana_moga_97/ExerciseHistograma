
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ana
 */
public class PgmImage {

    BufferedReader br = null;

    public PgmImage(String nume_fisier) {
        try {
            this.br = new BufferedReader(new FileReader(nume_fisier));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PgmImage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<CharOccurence> customReadPGM() throws InvalidArgumentException {

        Scanner s = new Scanner(this.br);
        int width = s.nextInt();
        int height = s.nextInt();
        if (width < 1 && width > 2160) {
            throw new InvalidArgumentException("Valoare de intrare invalida");
        }
        if (height < 1 && height > 3840) {
            throw new InvalidArgumentException("Valoare de intrare invalida");
        }
        int i;
        List<CharOccurence> ourlist = (List<CharOccurence>) new ArrayList<CharOccurence>();
        HashMap<Integer, Integer> map = (HashMap<Integer, Integer>) new HashMap<Integer, Integer>();
        for (i = 0; i < width * height; i++) {
            int colourGrey = s.nextInt();
            if (colourGrey >= 0 && colourGrey <= 255) {
                if (map.containsKey(colourGrey)) {
                    map.put(colourGrey, map.get(colourGrey) + 1);
                } else {
                    map.put(colourGrey, 1);
                }
            } else {
                System.out.print("Valoare de intrare invalida");
            }
        }
        for (Map.Entry obj : map.entrySet()) {
            CharOccurence e = new CharOccurence((int) obj.getKey(), (int) obj.getValue());
            ourlist.add(e);
        }
        return ourlist;
    }
}
