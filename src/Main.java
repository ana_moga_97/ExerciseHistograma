
import java.util.Collections;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ana
 */
public class Main {

    public static void main(String[] args) {
        try {
            PgmImage instanceObject = new PgmImage("input.txt");

            List<CharOccurence> ourList = instanceObject.customReadPGM();
            for (CharOccurence item : ourList) {
                System.out.print(item.toString());
            }
            Collections.sort(ourList, new OccurenceComparator());
            for (int i = 0; i < ourList.size(); i++) {
                CharOccurence curent = ourList.get(i);
                CharOccurence e1 = null;
                CharOccurence e2 = null;
                for (int j = 0; j < ourList.size(); j++) {
                    if (ourList.get(j).getVal() == curent.getVal() - 1) {
                        e1 = ourList.get(j);
                    }
                    if (ourList.get(j).getVal() == curent.getVal() + 1) {
                        e2 = ourList.get(j);
                    }
                    if (e1 != null && e2 != null) {
                        int maxim = Math.max(Math.max(e1.getOcc(), e2.getOcc()), curent.getOcc());
                        if (maxim == e1.getOcc()) {
                            System.out.print(e1.getVal());
                        }
                        if (maxim == e2.getOcc()) {
                            System.out.print(e2.getVal());
                        }
                        if (maxim == curent.getOcc()) {
                            System.out.print(curent.getVal());
                        }
                    } else if (e1 != null) {
                        int maxim = Math.max(e1.getOcc(), curent.getOcc());
                        if (maxim == e1.getOcc()) {
                            System.out.print(e1.getVal());
                        }
                        if (maxim == curent.getOcc()) {
                            System.out.print(curent.getVal());
                        }
                    } else if (e2 != null) {
                        int maxim = Math.max(e2.getOcc(), curent.getOcc());
                        if (maxim == e2.getOcc()) {
                            System.out.print(e2.getVal());
                        }
                        if (maxim == curent.getOcc()) {
                            System.out.print(curent.getVal());
                        }
                    } else {
                        int maxim = curent.getOcc();
                        System.out.print(curent.getVal());
                    }

                }
            }
        } catch (InvalidArgumentException e) {
            System.out.print(e.getMessage());
        }
    }
}
