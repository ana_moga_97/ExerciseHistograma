public class CharOccurence {
    int greyValue = 0;
    int occurences = 0;
    
    public CharOccurence(int greyVal, int occ) {
        this.greyValue = greyVal;
        this.occurences = occ;
    }
    public int getOcc(){
        return this.occurences;
    }
    public int getVal(){
        return this.greyValue;
    }
    public String toString(){
            return "("+greyValue+","+occurences+")";
        }
}
